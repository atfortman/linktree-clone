<?php

include('functions.php');

define('FILE_ERROR_LOG', './social_error.log');
define('FILE_DEBUG_LOG', './social_debug.log');
define('FILE_INFO_LOG', './social_info.log');

$site_title = 'page title';
$css = 'assets/css/style.css';
$headshot = 'assets/img/me.jpg';
$fa_kit = 'URL to your font-awesome kit';
$intro = "Hey, I'm forty and here's where you can find me:";
$quote = 'some strong inspirational quote that helps you get by';
$facebook = new social(array('type' => 'facebook',
                            'handle' => 'atfortman'));
$twitter = new social(array('type' => 'twitter',
                            'handle' => 'adamfortman'));
$instagram = new social(array('type' => 'instagram',
                            'handle' => 'adamfortman'));
$google = new social(array('type' => 'google',
                            'handle' => 'atfortman@gmail.com'));
$discord = new social(array('type' => 'discord',
                            'handle' => '537086497778696193',
                            'display' => 'forty#6768'));
$links = array($facebook->getData(),
            $twitter->getData(),
            $instagram->getData(),
            $google->getData(),
            $discord->getData()
);