<?php

class social {
    function __construct(array $data) {
        if ( $data['type'] === 'facebook' ) {
            $this->name = 'facebook';
            $this->fa = 'fa-brands fa-facebook';
            $this->url = 'facebook.com/';
        }
        if ( $data['type'] === 'twitter' ) {
            $this->name = 'twitter';
            $this->fa = 'fa-brands fa-twitter';
            $this->url = 'twitter.com/';
        }
        if ( $data['type'] === 'instagram') {
            $this->name = 'instagram';
            $this->fa = 'fa-brands fa-instagram';
            $this->url = 'instagram.com/';
        }
        if ( $data['type'] === 'google' ) {
            $this->name = 'google';
            $this->fa = 'fa-brands fa-google';
            $this->url = 'mailto:';
        }
        if ( $data['type'] === 'discord' ) {
            $this->name = 'discord';
            $this->fa = 'fa-brands fa-discord';
            $this->url = 'discord.com/users/';
        }
        if ( $data['type'] === 'polywork' ) {
            $this->name = 'polywork';
            $this->fa = 'fa-solid fa-hashtag';
            $this->url = 'polywork.com/';
        }
        if ( $data['type'] === 'goodreads' ) {
            $this->name = 'goodreads';
            $this->fa = 'fa-brands fa-goodreads';
            $this->url = 'goodreads.com/';
        }
        if ( $data['type'] === 'letterboxd' ) {
            $this->name = 'letterboxd';
            $this->fa = 'fa-solid fa-film';
            $this->url = 'letterboxd.com/';
        }
        if ( $data['type'] === 'discogs' ) {
            $this->name = 'discogs';
            $this->fa = 'fa-solid fa-record-vinyl';
            $this->url = 'discogs.com/user/';
        }

        if ( isset($data['display']) ) {
            $this->display = $data['display'];
        }
        $this->handle = $data['handle'];
    }

    public function getData() {
        $data = array('name' => $this->name,
                        'fa' => $this->fa,
                        'url' => $this->url,
                        'handle' => $this->handle);
        if ( isset($this->display) ) {
            $data['display'] = $this->display;
        }
        return $data;
    }
}


/**
 * Log errors to error and info log as defined in config.php
 *
 * @param string $msg Error message to print.
 * @param bool $trace (Optional) If true, add backtrace to error.
 */
function logError( $msg, $trace = false ) {

    $time = date( 'Y-m-d H:i:s' );
    $str = print_r( $msg, true ) . "\n";
    if ( $trace ) {
        $str .= getBacktrace() . "\n";
    }
    logDebug('ERROR: ' . $str);
    file_put_contents( FILE_ERROR_LOG, $time . ' ' . $str, FILE_APPEND );
}

/**
 * Logs debug info to debug log as defined in config.php
 *
 * @param string $msg Debug message to print
 * @param null $data (Optional) Array to print after output
 */
function logDebug( $msg, $data = null ) {
    $str = date( 'Y-m-d H:i:s' ) . ' ' . print_r( $msg, true ) . "\n";
    if ( $data ) {
        $str .= print_r( $data, true ) . "\n";
    }
    file_put_contents( FILE_DEBUG_LOG, $str, FILE_APPEND );
}

/**
 * Logs informational messages to the info and debug logs as defined in config.php
 *
 * @param string $msg Info message to print
 */
function logInfo( $msg ) {
    $str = date( 'Y-m-d H:i:s' ) . ' ' . print_r( $msg, true ) . "\n";
    file_put_contents( FILE_DEBUG_LOG, $str, FILE_APPEND );
    file_put_contents( FILE_INFO_LOG, $str, FILE_APPEND );
}

function getBacktrace() {
    return getExceptionTraceAsString(new Exception);
}

function getExceptionTraceAsString(Exception $exception) {
    $rtn = '';
    $count = 0;
    foreach ($exception->getTrace() as $frame) {
        $args = '';
        if (isset($frame['args'])) {
            $args = array();
            foreach ($frame['args'] as $arg) {
                if (is_string($arg)) {
                    $args[] = "'" . $arg . "'";
                } elseif (is_array($arg)) {
                    $args[] = 'Array';
                } elseif ($arg === null) {
                    $args[] = 'NULL';
                } elseif (is_bool($arg)) {
                    $args[] = $arg ? 'true' : 'false';
                } elseif (is_object($arg)) {
                    $args[] = get_class($arg);
                } elseif (is_resource($arg)) {
                    $args[] = get_resource_type($arg);
                } else {
                    $args[] = $arg;
                }
            }
            $args = implode(', ', $args);
        }
        $rtn .= sprintf( "#%s %s(%s): %s(%s)\n",
            $count,
            $frame['file'],
            $frame['line'],
            $frame['function'],
            $args );
        $count++;
    }
    return $rtn;
}