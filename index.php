<!doctype html>
<html>
    <?php
        include("config.php");
    ?>
    <head>
        <title>whereis forty | home</title>
        <link rel="stylesheet" href="<?= $css; ?>">
        <script src="<?= $fa_kit; ?>" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
        <div class="head__section">
            <img src="<?= $headshot; ?>" alt="">
            <p class="intro"><?= $intro; ?></p>
        </div>
        <div class="social__links">
            <ul id="item">
                <?php foreach ( $links as $link ) { ?>
                    <li class="<?= $link["name"]; ?>">
                        <?php if ( $link["url"] == "mailto:" ) { ?>
                            <a href="<?= $link["url"] . $link["handle"];?>" target="_blank">
                        <?php } else { ?>
                            <a href="https://<?= $link["url"] . $link["handle"]; ?>" target="_blank">
                        <?php } ?>
                            <i class="<?= $link["fa"]; ?>"></i>
                            <span><h3>
                            <?php if ( $link["display"] ) {
                                echo $link["display"];
                            } elseif ( $link["url"] == "mailto:" ) {
                                echo $link["handle"];
                            } else {
                                echo "@" . $link["handle"];
                            } ?>
                            </h3></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <footer>
            <p><em><?= $quote; ?></em></p>
        </footer>
        </div>
    </body>
</html>